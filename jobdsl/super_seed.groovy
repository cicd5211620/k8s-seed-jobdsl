job('super-seed') {
  scm {
    git {
      remote {
        url ('https://gitlab.com/cicd5211620/k8s-pipeline.git')
      }
    }
  }
  steps {
    dsl {
      external('jobdsl/**/*.groovy')
      removeAction('DELETE')
    }
  }
}