pipelineJob('backend') {
  definition {
    cpsScm {
        scm {
          git {
            remote {
              url ('https://gitlab.com/cicd5211620/k8s-pipeline.git')
            }
          }
        }
        scriptPath("backend/Jenkinsfile")
    }
  }
}